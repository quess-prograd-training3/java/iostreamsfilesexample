package com.example3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CopyMain {
    public static void main(String[] args) {
        CopyFiles copyFiles=new CopyFiles();
        copyFiles.fileWrite();
        copyFiles.copyByteByByte();
        copyFiles.copyAllContent();
    }
}
