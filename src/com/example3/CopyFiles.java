package com.example3;

import jdk.nashorn.internal.runtime.ECMAException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class CopyFiles {
    public void fileWrite(){
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter("G:\\file1.txt"));
            writer.write("This is the first line.");
            writer.newLine();
            writer.write("This is the second line.");
            writer.newLine();
            writer.write("This is the third line.");
            writer.close();
            System.out.println("File is create and written sucessfull");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }

    public void copyByteByByte(){
        try {
            FileInputStream in = new FileInputStream("G:\\file1.txt");
            FileOutputStream out = new FileOutputStream("G:\\file2.txt");
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
            in.close();
            out.close();
            System.out.println("Copy files1");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
    public void copyAllContent() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("G:\\file1.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter("G:\\file3.txt"));
            String line;
            while ((line = reader.readLine()) != null) {
                writer.write(line);
                writer.newLine();
            }
            reader.close();
            writer.close();
            System.out.println("Copy files2");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }
}
