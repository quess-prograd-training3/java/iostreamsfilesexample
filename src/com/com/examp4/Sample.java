package com.com.examp4;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
public class Sample {
        public static  void main(String[] args) {
            // Create a text file and write a few lines in it
            try {
                FileWriter fileWriter = new FileWriter("file1.txt");
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write("Line 1\n");
                bufferedWriter.write("Line 2\n");
                bufferedWriter.write("Line 3\n");
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Create 2 files and copy the content of one file to another, byte by byte
            try {
                FileInputStream fileInputStream = new FileInputStream("file1.txt");
                 FileOutputStream fileOutputStream = new FileOutputStream("file2.txt");
                int c;
                while ((c = fileInputStream.read()) != -1) {
                    fileOutputStream.write(c);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Create 2 files and copy the content of one file to another, all at once
            try {
                FileReader fileReader = new FileReader("file1.txt");
                 BufferedReader bufferedReader = new BufferedReader(fileReader);
                 FileWriter fileWriter = new FileWriter("file3.txt");
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    bufferedWriter.write(line);
                    bufferedWriter.newLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

